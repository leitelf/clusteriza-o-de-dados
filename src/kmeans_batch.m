function [W, V, SSD] = kmeans_batch (X, K, it, rep)
  for j = 1:rep
    Waux = init_prototypes(X, K);
    
    SSDaux = [];
    for i = 1:it
      Vaux = get_closest(X, Waux);
      Waux = compute_prototypes(X, Vaux, K, Waux);
      SSDaux(i) = compute_error(X, Vaux, Waux);
      if (i > 4 && SSDaux(i) == SSDaux(i-1) && SSDaux(i) == SSDaux(i-2))
        break;
      endif
    endfor
    
    SSDj(j) = SSDaux(end);
    Wj{j} = Waux;
    Vj{j} = Vaux;
  endfor
  
  [SSD, best_idx] = min(SSDj);
  W = Wj{best_idx};
  V = Vj{best_idx};
endfunction


function [W] = init_prototypes (X, K)
  W = zeros(K, size(X, 2));
  W = X(randperm(size(X, 1))(1:K), :);
endfunction

function [V] = get_closest (X, W)
  D = pdist2(X, W, 'sqeuclidean');
  for i = 1:size(X, 1)
    [dump, V(i)] = min(D(i, :));
  endfor
endfunction

function [W] = compute_prototypes (X, V, K, Wold)
  W = Wold;
  for i = 1:K
    Vi = X(V == i, :);
    if (size(Vi, 1) >= 1)
      W(i, :) = mean(Vi);
    endif
    
  endfor
endfunction

function [SSD] = compute_error (X, V, W)
  N = size(X, 1);
  SSD = 0;
  for i = 1:N
    dist = norm(X(i, :) - W(V(i), :));
    SSD = SSD + (dist * dist);
  endfor
  
endfunction
