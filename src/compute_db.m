% DB = Indice Davies Bouldin
% X = conjunto de amostras
% W = prototipos
% V = particoes

% Menor melhor
function DB = compute_db(X, V, W)
  K = size(W, 1);
  
  R = [];
  
  for i = 1:K
    R = [R; compute_R(X, V, W, i, 1, 2)];
  endfor
  
  DB = mean(R);
endfunction

function s = compute_s (Vi, Wi, q)
  Ni = size(Vi, 1);
  
  s_aux = Vi - Wi;
  
  s = 0;
  for n = 1:Ni
    s = s + (norm(s_aux(n) ^ q));
  endfor
  s = s/Ni;
  s = s ^ (1/q);
endfunction

function R = compute_R (X, V, W, i, q, t)
  K = size(W, 1);
  Raux = [];

  Xi = X(V==i, :);
  si = compute_s(X(V==i, :), W(i, :), q);
  
  
  for j = 1:K
    if (j != i)
      Xj = X(V==j, :);
      Wj = W(j, :);
      aux =  (si + compute_s(Xj, Wj, q)) / norm(W(i, :) - Wj, t);
      Raux = [Raux; aux];
    endif
  endfor
  
  R = max(Raux);
  
endfunction
