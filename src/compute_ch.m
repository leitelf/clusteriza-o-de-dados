% CH = Indice Calinski Harabasz
% X = conjunto de amostras
% W = prototipos
% V = particoes

% Maior melhor
function CH = compute_ch(X, V, W)
  K = size(W, 1);
  N = size(X, 1);
  
  Bk = compute_Bk(mean(X), V, W);
  Wk = compute_Wk(X, V, W);
  
  CH = (Bk / (K - 1))/ (Wk / (N - K));
  
endfunction

function Bk = compute_Bk(C, V, W)
  K = size(W, 1);
  Bk = 0;
  for i = 1:K
    Ni = sum(V == i);
    aux = (W(i, :) - C);
    Bk = Bk + (Ni * (aux * aux'));
  endfor
endfunction

function Wk = compute_Wk(X, V, W)
  K = size(W, 1);
  Wk = 0;
  for i = 1:K
    Wi = W(i, :);
    Vi = (X(V==i, :));
    for l = 1:size(Vi, 1)
      xl = Vi(l, :);
      aux = xl - Wi;
      Wk = Wk + (aux * aux');
    endfor
  endfor
endfunction
