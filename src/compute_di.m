% DI = Indice Dunn
% X = conjunto de amostras
% W = prototipos
% V = particoes

% Maior melhor
function DI = compute_di(X, V, W)

  K = size(W, 1);
  d = [];
  D = [];
  for i = 1:K
    for j = i+1:K
      if (j == i)
        continue;
      endif
      d = [d; delta(X(V==i, :), X(V==j, :))];
    endfor
    D = [D; DELTA(X(V==i, :))];
  endfor

  dmin = min(d);
  Dmax = max(D);
  DI = dmin / Dmax;
endfunction

function d = delta (Vi, Vj)
  dist = pdist2(Vi, Vj, 'euclidean');
  d = min(min(dist));
endfunction

function D = DELTA (Vl)
  D = max(pdist(Vl, 'euclidean'));
endfunction
