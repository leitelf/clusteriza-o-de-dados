function [Xnorm] = norm_data(X)

  cols = size(X, 2);
  Xnorm = X;
  for i = 1:cols
    xmean = mean(X(:, i));
    xstd = std(X(:, i));
    Xnorm(:, i) = Xnorm(:, i) - xmean;
    Xnorm(:, i) = Xnorm(:, i) / xstd;    
  endfor
endfunction
